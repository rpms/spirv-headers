%global commit 4f7b471f1a66b6d06462cd4ba57628cc0cd087d7
%global shortcommit %(c=%{commit}; echo ${c:0:7})


Name:           spirv-headers
Version:        1.5.5
Release:        6%{?dist}
Summary:        Header files from the SPIR-V registry

License:        MIT
URL:            https://github.com/KhronosGroup/SPIRV-Headers/
Source0:        %{url}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildArch:      noarch

BuildRequires:  cmake3
BuildRequires:  ninja-build
BuildRequires:  gcc
BuildRequires:  gcc-c++

%description
%{summary}

This includes:

* Header files for various languages.
* JSON files describing the grammar for the SPIR-V core instruction
  set, and for the GLSL.std.450 extended instruction set.
* The XML registry file

%package        devel
Summary:        Development files for %{name}

%description    devel
%{summary}

This includes:

* Header files for various languages.
* JSON files describing the grammar for the SPIR-V core instruction
  set, and for the GLSL.std.450 extended instruction set.
* The XML registry fil

%prep
%autosetup -n SPIRV-Headers-%{commit}
chmod a-x include/spirv/1.2/spirv.py


%build
%cmake3 -DCMAKE_INSTALL_LIBDIR=%{_lib} -GNinja
%cmake_build

%install
%cmake_install

%files devel
%license LICENSE
%doc README.md
%{_includedir}/spirv/
%{_datadir}/cmake/SPIRV-Headers/*.cmake
%{_datadir}/pkgconfig/SPIRV-Headers.pc

%changelog
* Tue Sep 10 2024 José Expósito <jexposit@redhat.com> - 1.5.5-6
- Update to 1.3.283.0 SDK
  Resolves: https://issues.redhat.com/browse/RHEL-54284

* Wed Jul 12 2023 Dave Airlie <airlied@redhat.com> - 1.5.5-5
- Update to spirv headers for 1.3.250.1 sdk

* Mon Feb 13 2023 Dave Airlie <airlied@redhat.com> - 1.5.5-4
- Update to spirv headers for 1.3.239.0 sdk

* Wed Aug 24 2022 Dave Airlie <airlied@redhat.com> - 1.5.5-3
- Update to spirv headers for 1.3.224 sdk

* Mon Jun 20 2022 Dave Airlie <airlied@redhat.com> - 1.5.5-2
- Update to spirv headers for 1.3.216 sdk

* Mon Feb 21 2022 Dave Airlie <airlied@redhat.com> - 1.5.5-1.20220117.gitb42ba6d
- Update to spirv headers for 1.3.204 sdk

* Thu Jan 28 2021 Dave Airlie <airlied@redhat.com> - 1.5.4-1.20201128.gitf027d53
- Update to spirv headers for 1.2.162 sdk

* Tue Aug 04 2020 Dave Airlie <airlied@redhat.com> - 1.5.1-3
- Update to latest upstream snapshot

* Wed Jan 29 2020 Dave Airlie <airlied@redhat.com> - 1.5.1-2
- Update to latest upstream snapshot

* Tue Nov 12 2019 Dave Airlie <airlied@redhat.com> - 1.5.1-1
- Update to latest upstream snapshot

* Sat Aug 03 2019 Dave Airlie <airlied@redhat.com> - 1.4.2-0.1
- Update to latest upstream snapshot

* Thu Mar 07 2019 Dave Airlie <airlied@redhat.com> - 1.2-0.8.20190307.git03a0815
- Update to latest version

* Mon Jul 23 2018 Leigh Scott <leigh123linux@googlemail.com> - 1.2-0.7.20180703.gitff684ff
- Update for SPIRV-Tools-2018.4

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-0.6.20180405.git12f8de9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Apr 24 2018 Leigh Scott <leigh123linux@googlemail.com> - 1.2-0.5.20180405.git12f8de9
- Update for vulkan 1.0.73.0

* Fri Feb 09 2018 Leigh Scott <leigh123linux@googlemail.com> - 1.2-0.4.20180201.gitce30920
- Update for vulkan 1.0.68.0

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-0.3.20171015.git0610978
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Jan 22 2018 Leigh Scott <leigh123linux@googlemail.com> - 1.2-0.2.20171015.git0610978
- fix rpmlint error

* Thu Jul 13 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.2-0.1.20171015.git0610978
- First build

